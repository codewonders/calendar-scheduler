/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */

import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

/* -------------------------- Internal Dependencies ------------------------- */
import ErrorBoundary from 'components/error-boundary';
import SkipToMain from 'components/a11y/skip-to-main';

/* ------------------------- Component Dependencies ------------------------- */
import Home from 'pages/home';

/* ---------------------------- Routes PropTypes ---------------------------- */

const propTypes = {
	location: PropTypes.any,
};

const routes = ({ location }) => (
	<ErrorBoundary>
		<SkipToMain content="main" />
		<main id="main">
			<Switch location={location}>
				<Route exact path="/" component={Home} />
			</Switch>
		</main>
	</ErrorBoundary>
);

routes.propTypes = propTypes;

export default withRouter(routes);
