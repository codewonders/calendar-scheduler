/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React from 'react';

/* -------------------------- Internal Dependencies ------------------------- */

import Routes from './routes';

const App = () => {
	return <Routes />;
};

export default App;
