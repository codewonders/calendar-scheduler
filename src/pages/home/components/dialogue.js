/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/* -------------------------- Internal Dependencies ------------------------- */
import useLocalStorage from 'hooks/useLocalStorage';
import history from 'utils/history';

/* --------------------------- Image Dependencies --------------------------- */
import { ReactComponent as Confirm } from 'assets/icons/icon-check.svg';

/* --------------------------- Dialogue PropTypes --------------------------- */
const propTypes = {
	show: PropTypes.bool,
	onHide: PropTypes.func,
	data: PropTypes.object,
};

const Dialogue = ({ show, onHide, data }) => {
	const [message, setMessage] = useState('');
	const [session, saveEventSessions] = useLocalStorage('SESSIONS', []);

	const saveSession = async () => {
		if (!message) return alert('Reason for this call is required');

		const newData = {
			...data,
			reason: message,
		};
		await saveEventSessions([...session, newData]);
		alert('You have successfully scheduled a session');

		history.go();
	};

	return (
		<ModalLayout show={show} onHide={onHide}>
			<Modal.Header closeButton></Modal.Header>
			<Modal.Body>
				<div className="text-center">
					<Confirm />
					<p>Your call is about to be scheduled for</p>
					<h4>
						{data?.from} - {data?.to}, {data?.date}
					</h4>
				</div>

				<textarea
					onChange={(e) => setMessage(e.target.value)}
					value={message}
					className="form-control"
					placeholder="Reason for this call"
				></textarea>
				<button
					className="btn btn-careerfoundry ml-auto d-block mt-3"
					onClick={() => saveSession()}
				>
					Schedule Session
				</button>
			</Modal.Body>
		</ModalLayout>
	);
};

const ModalLayout = styled(Modal)`
	.modal-body {
		padding: 2.2rem;
	}
	.modal-header {
		border: none;
		padding-bottom: 0;
	}
	p {
		font-size: 15px;
		font-weight: 500;
		color: #676767;
		margin-bottom: 2px;
	}
	h4 {
		font-size: 20px;
		font-weight: 700;
		color: var(--black);
		margin-bottom: 1.5rem;
	}
	svg {
		width: 50px;
		height: 50px;
		fill: #3b9230;
		margin-bottom: 1rem;
	}
	textarea {
		font-size: 15px;
		padding: 15px;
		border: 1px solid #e6e6e6;
		resize: none;
	}
	button.btn {
		font-size: 14px;
		padding: 10px 36px;
		font-weight: 600;

		background: #3b9a2f;
	}
`;

Dialogue.propTypes = propTypes;
export default Dialogue;
