/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React, { useState } from 'react';
import 'react-calendar/dist/Calendar.css';
import { format } from 'date-fns';

/* -------------------------- Internal Dependencies ------------------------- */
import generateTimeSeries from 'utils';
import Dialogue from './components/dialogue';
import useLocalStorage from 'hooks/useLocalStorage';

/* --------------------------- Styles Dependencies -------------------------- */
import {
	CalendarWrapper,
	Card,
	Logo,
	CalendarSlot,
	TimeSlots,
	Button,
	BottomBar,
} from './styles';

/* --------------------------- Image Dependencies --------------------------- */
import CFLogo from 'assets/icons/cf-logo.png';

const Home = () => {
	const [timeSlots] = useState(generateTimeSeries());
	const [session] = useLocalStorage('SESSIONS', []);

	const [calDate, setCalDate] = useState(null);
	const [time, setTime] = useState({
		from: '',
		to: '',
	});
	const [show, setShow] = useState(false);

	const onChange = (slot) => {
		setCalDate(slot);
		setTime('');
	};

	const takenSlots = session
		?.filter(
			(ses) =>
				new Date(ses._date).toDateString() === new Date(calDate).toDateString()
		)
		.map((status) => {
			return status.from;
		});

	const getTimeSlotByIndex = (slot) => {
		if (!slot) return;

		const currentIndex = timeSlots.indexOf(slot);
		const nextIndex = (currentIndex + 1) % timeSlots.length;

		return timeSlots[nextIndex];
	};

	return (
		<CalendarWrapper>
			<div className="container">
				<div
					className={`${
						!calDate ? 'col-md-10 col-lg-10' : 'col-md-12 col-lg-12'
					} m-auto transition`}
				>
					<Card isDate={calDate} className="d-block d-lg-flex">
						<div className="mentor__container">
							<Logo src={CFLogo} />
							<h4>Book an appointment your mentor.</h4>
							<img
								src="https://images.pexels.com/photos/1073097/pexels-photo-1073097.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
								alt="Max"
								className="avatar"
							/>
							<h2>Max Mustermann</h2>
							<p className="course-info">Software Engineer Mentor</p>
						</div>
						<div className="calendar__container">
							<h3>Select a Date & Time</h3>

							<div className="d-lg-flex d-block">
								<CalendarSlot
									onChange={onChange}
									next2Label={null}
									prev2Label={null}
									showNeighboringMonth={false}
									minDate={new Date()}
									value={calDate ?? new Date()}
								/>
								{calDate && (
									<TimeSlots>
										<p>Choose a time for your session</p>
										<h4>{format(new Date(calDate), 'eeee, MMMM do')}</h4>
										{timeSlots.map((slot) => {
											return (
												<Button
													isActive={slot === time.from}
													isAnHour={
														time.from && slot === getTimeSlotByIndex(time?.from)
													}
													onClick={() => {
														if (takenSlots?.includes(slot))
															return alert(
																'Error: This time slot has been allocated please choose a new time slot.'
															);

														return setTime((prev) => {
															return {
																...prev,
																from: slot,
																to: getTimeSlotByIndex(slot),
															};
														});
													}}
													disabled={takenSlots?.includes(slot)}
												>
													{slot}
												</Button>
											);
										})}
									</TimeSlots>
								)}
							</div>
						</div>
					</Card>
					{calDate && time ? (
						<BottomBar>
							<button
								className="btn btn-default"
								onClick={() => onChange(null)}
							>
								{'< Back '}
							</button>
							<button
								className="btn btn btn-careerfoundry"
								onClick={() => setShow(true)}
							>
								Confirm Date & Time
							</button>
						</BottomBar>
					) : null}
				</div>
			</div>
			<Dialogue
				show={show}
				onHide={() => setShow(false)}
				data={{
					date: format(new Date(calDate), 'eeee, MMMM do'),
					_date: calDate,
					...time,
				}}
			/>
		</CalendarWrapper>
	);
};
export default Home;
