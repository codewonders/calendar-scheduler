/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import styled, { css } from 'styled-components';
import Calendar from 'react-calendar';

export const CalendarWrapper = styled.div`
	background: var(--white);
	min-height: 100vh;
	padding: 3rem 0;
	display: flex;
	align-items: center;

	.transition {
		transition: all 0.2s ease-in;
		will-change: width;
	}
`;
export const Card = styled.div`
	background: #fff;
	border-radius: 8px;
	box-shadow: 0 2px 15px #38383812;
	border: 1px solid #efeeee;
	min-height: 500px;

	will-change: flex-flow;

	.mentor__container {
		flex: ${({ isDate }) => (isDate ? '0 0 30%' : '0 0 40%')};
		padding: 1.7rem;
		border-right: 1px solid #efeeee;

		img.avatar {
			width: 88px;
			height: 88px;
			padding: 2px;
			border: 2.2px solid #233c5021;
			border-radius: 50%;
			object-fit: cover;
		}
		h2 {
			font-weight: 600;
			color: #1d0a05;
			font-size: 30px;
			margin-top: 1rem;
			font-style: normal;
		}
		h4 {
			margin-bottom: 3rem;
			font-size: 17px;
			font-weight: 600;
			color: #616161;
			font-style: normal;
		}
		p {
			font-size: 15px;
			color: #6b6b6b;
		}
	}
	.calendar__container {
		flex: ${({ isDate }) => (isDate ? '0 0 70%' : '0 0 60%')};
		padding: 1.7rem;

		h3 {
			font-size: 20px;
			font-weight: 600;
			margin-bottom: 3rem;
			color: var(--black);
		}
	}
`;
export const CalendarSlot = styled(Calendar)`
	width: 100%;
	border: none;
	font-family: var(--font-primary);
	.react-calendar__navigation__label__labelText {
		font-size: 17px;
		font-weight: 600;
	}
	abbr {
		text-decoration: none;
	}
	.react-calendar__month-view__days__day--weekend {
		color: var(--theme-primary);
	}

	.react-calendar__tile {
		margin: 2px;
		font-size: 15px;
		flex-basis: 13.2857% !important;
		max-width: 13.2857% !important;
		overflow: hidden;
		border-radius: 8px;
		font-weight: 500;
		padding: 1.2em 0.5em;
		&:hover,
		&:focus {
			background: var(--theme-primary);
			color: white;
			transition: all 0.3s ease;
		}
		&:disabled {
			background-color: #fcfcfc;
			opacity: 0.67;
			cursor: not-allowed;
			&:hover {
				background-color: #fcfcfc;
				color: #d1d1d1;
			}
		}
		&--now {
			background: #e1eaf0;
			color: var(--theme-primary);
		}
		&--active {
			background: var(--theme-primary);
			color: white;
		}
	}

	.react-calendar__year-view__months__month {
		flex-basis: 32.3333% !important;
		max-width: 32.3333% !important;
		overflow: hidden;
	}
`;

export const Logo = styled.img`
	width: 37px;
	margin-bottom: 1.3rem;
`;

export const TimeSlots = styled.aside`
	@media (min-width: 990px) {
		flex: 55%;
		max-width: 55%;
	}
	padding: 0 0rem 0 1.5rem;
	p {
		font-size: 13px;
		margin-bottom: 3px;
		color: #8e8e8e;
		font-weight: 600;
		@media (max-width: 990px) {
			margin-top: 2rem;
		}
	}
	h4 {
		font-size: 16px;
		font-weight: 500;
		margin-bottom: 1.5rem;
	}
`;

export const Button = styled.button`
	border: 1px solid var(--theme-primary);
	color: var(--theme-primary);
	font-weight: 600;
	padding: 10px;
	font-size: 15px;
	display: block;
	width: 100%;
	margin-bottom: 0.6rem;
	transition: all 0.2s ease-in;
	background: transparent;
	vertical-align: middle;
	-webkit-user-select: none;
	user-select: none;

	line-height: 1.5;
	border-radius: 0.25rem;
	transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
		border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
	&:disabled {
		opacity: 0.25;
		cursor: not-allowed;
		&:hover {
			opacity: 0.25;
		}
	}

	${({ isActive }) =>
		isActive &&
		css`
			background: var(--theme-primary);
			color: #fff;
			&:before {
				content: 'Start Session';
				font-size: 12px;
				display: block;
				color: #cccccc;
				font-weight: 500;
			}
		`}
	${({ isAnHour }) =>
		isAnHour &&
		css`
			background: #d3e2ea;
			color: var(--theme-primary);
			&:before {
				content: 'End Session';

				font-size: 12px;
				display: block;
				color: var(--theme-primary);
				font-weight: 500;
			}
		`}
	&:hover {
		background: var(--theme-primary);
		color: #fff;
	}
`;

export const BottomBar = styled.div`
	position: sticky;
	width: 100%;
	background: white;
	margin-top: -1.8rem;
	border-radius: 0px 0 10px 10px;
	bottom: 0;
	padding: 16px 32px;
	display: flex;
	justify-content: flex-end;
	border: 1px solid #efeeee;
	button {
		font-size: 14px;
		padding: 10px 36px;
		font-weight: 600;
		@media (max-width: 990px) {
			padding: 10px 20px;
		}
		&.btn-careerfoundry {
			background: #3b9a2f;
		}
		&.btn-default {
			text-decoration: underline;
		}
	}
`;
