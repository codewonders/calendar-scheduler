const generateTimeSeries = () => {
	const date = new Date(2021, 0, 1);
	const timeSlots = [];

	while (date.getDate() === 1) {
		timeSlots.push(date.toLocaleTimeString('en-US'));
		date.setMinutes(date.getMinutes() + 60);
	}
	return timeSlots;
};

export default generateTimeSeries;
